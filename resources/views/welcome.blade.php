<!doctype html>
<html class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&amp;subset=devanagari,latin-ext" rel="stylesheet">
        
        <!-- title of site -->
        <title>sitinoraika</title>

        <!-- For favicon png -->
        <link rel="shortcut icon" type="image/icon" href="{{ asset('/assets/logo/logo5.png') }}"/>
       
        <!--font-awesome.min.css-->
        <link rel="stylesheet" href="{{ asset('/assets/css/font-awesome.min.css') }}">

        <!--flat icon css-->
        <link rel="stylesheet" href="{{ asset('/assets/css/flaticon.css') }}">

        <!--animate.css-->
        <link rel="stylesheet" href="{{ asset('/assets/css/animate.css') }}">

        <!--owl.carousel.css-->
        <link rel="stylesheet" href="{{ asset('/assets/css/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/assets/css/owl.theme.default.min.css') }}">
    
        <!--bootstrap.min.css-->
        <link rel="stylesheet" href="{{ asset('/assets/css/bootstrap.min.css') }}">
    
        <!-- bootsnav -->
        <link rel="stylesheet" href="{{ asset('/assets/css/bootsnav.css') }}" > 
        
        <!--style.css-->
        <link rel="stylesheet" href="{{ asset('/assets/css/style.css') }}">
        
        <!--responsive.css-->
        <link rel="stylesheet" href="{{ asset('/assets/css/responsive.css') }}">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
  
  <body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
    
    <!-- top-area Start -->
    <header class="top-area">
      <div class="header-area">
        <!-- Start Navigation -->
          <nav class="navbar navbar-default bootsnav navbar-fixed dark no-background">

              <div class="container">

                  <!-- Start Header Navigation -->
                  <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                          <i class="fa fa-bars"></i>
                      </button>
                      <a class="navbar-brand" href="index.html">{noraika}</a>
                  </div><!--/.navbar-header-->
                  <!-- End Header Navigation -->

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse menu-ui-design" id="navbar-menu">
                      <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                      <li class=" smooth-menu active"></li>
                          <li class=" smooth-menu"><a href="#education">education</a></li>
                          <li class="smooth-menu"><a href="#skills">skills</a></li>
                          <li class="smooth-menu"><a href="#experience">experience</a></li>
                          <li class="smooth-menu"><a href="#profiles">profile</a></li>
                          <li class="smooth-menu"><a href="#portfolio">portfolio</a></li>
                          <li class="smooth-menu"><a href="#clients">projects</a></li>
                          <li class="smooth-menu"><a href="#contact">contact</a></li>
                      </ul><!--/.nav -->
                  </div><!-- /.navbar-collapse -->
              </div><!--/.container-->
          </nav><!--/nav-->
          <!-- End Navigation -->
      </div><!--/.header-area-->

        <div class="clearfix"></div>

    </header><!-- /.top-area-->
    <!-- top-area End -->
  
    <!--welcome-hero start -->
    <section id="welcome-hero" class="welcome-hero">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <div class="header-text">
              <h2>hi <span>,</span> i am <br> Siti <br> Noraika <span>.</span>   </h2>
              <p>UI/UX ENTHUSIAST </p>
              <a href="assets/download/cvika.pdf" download>download resume</a>
            </div><!--/.header-text-->
          </div><!--/.col-->
        </div><!-- /.row-->
      </div><!-- /.container-->

    </section><!--/.welcome-hero-->
    <!--welcome-hero end -->

    <!--about start -->
    <section id="about" class="about">
      <div class="section-heading text-center">
        <h2>about me</h2>
      </div>
      <div class="container">
        <div class="about-content">
          <div class="row">
            <div class="col-sm-6">
              <div class="single-about-txt">
                <h3>
                  I am a UI/UX Designer Enthusiast. 
                </h3>
                <p>
                  I am twenty years old . I student at Gadjah Mada University . My Majors 
                  is Computer and Information System Vocational UGM. I was born in JohorBohro,
                  Malaysia on June 20, 1998 . I like travelling and eat because If I don't eat,
                  I will die :). and My hometown in Cirebon , West Java and I am Cirebonese. I passionate in UI/UX designer, CSS , HTML .
                </p>
                <div class="row">
                  <div class="col-sm-4">
                    <div class="single-about-add-info">
                      <h3>phone</h3>
                      <p>0895-3209-03454</p>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="single-about-add-info">
                      <h3>email</h3>
                      <p>noraika.siti@gmail.com</p>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="single-about-add-info">
                      <h3>website</h3>
                      <p>www.brownsine.com</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-offset-1 col-sm-5">
              <div class="single-about-img">
                <img src="assets/images/about/ikayo.jpg" alt="ikayo">
                <div class="about-list-icon">
                  <ul>
                    <li>
                      <a href="https://www.facebook.com/ika.ikolpartiii">
                        <i  class="fa fa-facebook" aria-hidden="true"></i>
                      </a>
                    </li><!-- / li -->
                    <li>
                      <a href="https://dribbble.com/sitinoraika">
                        <i  class="fa fa-dribbble" aria-hidden="true"></i>
                      </a>
                      
                    </li><!-- / li -->
                    
                    <li>
                      <a href="https://www.linkedin.com/in/siti-noraika-aa2200135/">
                        <i  class="fa fa-linkedin" aria-hidden="true"></i>
                      </a>
                    </li><!-- / li -->
                    <li>
                      <a href="https://www.instagram.com/ikanoraika/?hl=id">
                        <i  class="fa fa-instagram" aria-hidden="true"></i>
                      </a>
                    </li><!-- / li -->
                    
                    
                  </ul><!-- / ul -->
                </div><!-- /.about-list-icon -->

              </div>

            </div>
          </div>
        </div>
      </div>
    </section><!--/.about-->
    <!--about end -->
    
    <!--education start -->
    <section id="education" class="education">
      <div class="section-heading text-center">
        <h2>education Formal</h2>
      </div>
      <div class="container">
        <div class="education-horizontal-timeline">
          <div class="row">
            <div class="col-sm-3">
              <div class="single-horizontal-timeline">
                <div class="experience-time">
                  <h2>2004-2010</h2>
                  <h3>Elementary<span> </span>School</h3>
                </div><!--/.experience-time-->
                <div class="timeline-horizontal-border">
                  <i class="fa fa-circle" aria-hidden="true"></i>
                  <span class="single-timeline-horizontal"></span>
                </div>
                <div class="timeline">
                  <div class="timeline-content">
                    <h4 class="title">
                    Kebon Baru V
                    <h5>Cirebon City, West Java</h5>
                    <p class="description">
                      
                    </p>
                  </div><!--/.timeline-content-->
                </div><!--/.timeline-->
              </div>
            </div>
            <div class="col-sm-3">
              <div class="single-horizontal-timeline">
                <div class="experience-time">
                  <h2>2010 - 2013</h2>
                  <h3>Junior High <span> </span>School</h3>
                </div><!--/.experience-time-->
                <div class="timeline-horizontal-border">
                  <i class="fa fa-circle" aria-hidden="true"></i>
                  <span class="single-timeline-horizontal"></span>
                </div>
                <div class="timeline">
                  <div class="timeline-content">
                    <h4 class="title">
                      SMPN 2 
                    </h4>
                    <h5>Cirebon City, West Java</h5>
                    <p class="description">
                    </p>
                  </div><!--/.timeline-content-->
                </div><!--/.timeline-->
              </div>
            </div>
            <div class="col-sm-3">
              <div class="single-horizontal-timeline">
                <div class="experience-time">
                  <h2>2013-2016</h2>
                  <h3>Senior High <span> </span>School</h3>
                </div><!--/.experience-time-->
                <div class="timeline-horizontal-border">
                  <i class="fa fa-circle" aria-hidden="true"></i>
                  <span class="single-timeline-horizontal spacial-horizontal-line
                  "></span>
                </div>
                <div class="timeline">
                  <div class="timeline-content">
                    <h4 class="title">
                     SMAN 1
                    </h4>
                    <h5>Cirebon City , West Java</h5>
                    <p class="description">
                      
                    </p>
                  </div><!--/.timeline-content-->
                </div><!--/.timeline-->
              </div>
            </div>
              <div class="col-sm-3">
              <div class="single-horizontal-timeline">
                <div class="experience-time">
                  <h2>2016 - Now</h2>
                  <h3>Gadjah Mada<span> </span>University</h3>
                </div><!--/.experience-time-->
                <div class="timeline-horizontal-border">
                  <i class="fa fa-circle" aria-hidden="true"></i>
                  <span class="single-timeline-horizontal"></span>
                </div>
                <div class="timeline">
                  <div class="timeline-content">
                    <h4 class="title">
                      Computer and Information System
                    </h4>
                    <h5>Yogyakarta City, Central Java</h5>
                    <p class="description">
                    </p>
                  </div><!--/.timeline-content-->
                </div><!--/.timeline-->
              </div>
            </div>
          </div>
        </div>
      </div>

    </section><!--/.education-->
    <!--education end -->
    <section id="education" class="education">
      <div class="section-heading text-center">
        <h2>education NonFormal</h2>
      </div>
      <div class="container">
        <div class="education-horizontal-timeline">
          <div class="row">
            <div class="col-sm-2">
              <div class="single-horizontal-timeline">
                <div class="experience-time">
                  <h2>2010-2013</h2>
                  <h3>Course English<span> </span>Language</h3>
                </div><!--/.experience-time-->
                <div class="timeline-horizontal-border">
                  <i class="fa fa-circle" aria-hidden="true"></i>
                  <span class="single-timeline-horizontal"></span>
                </div>
                <div class="timeline">
                  <div class="timeline-content">
                    <h4 class="title">
                    LBPP-LIA(Language and Professional Education Institution)
                    <h5>Cirebon City, West Java</h5>
                    <p class="description">
                      
                    </p>
                  </div><!--/.timeline-content-->
                </div><!--/.timeline-->
              </div>
            </div>
            <div class="col-sm-2">
              <div class="single-horizontal-timeline">
                <div class="experience-time">
                  <h2>2016 - 2017</h2>
                  <h3>English Cafe <span> </span> Yogyakarta</h3>
                </div><!--/.experience-time-->
                <div class="timeline-horizontal-border">
                  <i class="fa fa-circle" aria-hidden="true"></i>
                  <span class="single-timeline-horizontal"></span>
                </div>
                <div class="timeline">
                  <div class="timeline-content">
                    <h4 class="title">
                      
                    </h4>
                    <h5>Yogyakarta City, Central Java</h5>
                    <p class="description">
                    </p>
                  </div><!--/.timeline-content-->
                </div><!--/.timeline-->
              </div>
            </div>
            <div class="col-sm-2">
              <div class="single-horizontal-timeline">
                <div class="experience-time">
                  <h2>2016</h2>
                  <h3>Training Android <span> </span>Developer</h3>
                </div><!--/.experience-time-->
                <div class="timeline-horizontal-border">
                  <i class="fa fa-circle" aria-hidden="true"></i>
                  <span class="single-timeline-horizontal spacial-horizontal-line
                  "></span>
                </div>
                <div class="timeline">
                  <div class="timeline-content">
                    <h4 class="title">
                     28-29 September 2016
                    </h4>
                    <h5>Yogyakarta City, Central Java</h5>
                    <p class="description">
                      
                    </p>
                  </div><!--/.timeline-content-->
                </div><!--/.timeline-->
              </div>
            </div>
              <div class="col-sm-2">
              <div class="single-horizontal-timeline">
                <div class="experience-time">
                  <h2>2017</h2>
                  <h3>Training User<span> </span>Experience</h3>
                </div><!--/.experience-time-->
                <div class="timeline-horizontal-border">
                  <i class="fa fa-circle" aria-hidden="true"></i>
                  <span class="single-timeline-horizontal"></span>
                </div>
                <div class="timeline">
                  <div class="timeline-content">
                    <h4 class="title">
                      May -April
                    </h4>
                    <h5>Gadjah Mada University Yogyakarta City, Central Java</h5>
                    <p class="description">
                    </p>
                  </div><!--/.timeline-content-->
                </div><!--/.timeline-->
              </div>
            </div>
            <div class="col-sm-2">
              <div class="single-horizontal-timeline">
                <div class="experience-time">
                  <h2>2018</h2>
                  <h3>Binar Academy<span> </span>UI/UX DESIGN</h3>
                </div><!--/.experience-time-->
                <div class="timeline-horizontal-border">
                  <i class="fa fa-circle" aria-hidden="true"></i>
                  <span class="single-timeline-horizontal"></span>
                </div>
                <div class="timeline">
                  <div class="timeline-content">
                    <h4 class="title">
                      September-Desember
                    </h4>
                    <h5> Damai 89 Street,Sariharjo, Ngaglik, Sleman ,D.I Yogyakarta, 55581</h5>
                    <p class="description">
                    </p>
                  </div><!--/.timeline-content-->
                </div><!--/.timeline-->
              </div>
            </div>
          </div>
        </div>
      </div>

    </section><!--/.education-->

    <!--skills start -->
    <section id="skills" class="skills">
        <div class="skill-content">
          <div class="section-heading text-center">
            <h2>skills</h2>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-md-4">
                <div class="single-skill-content">
                  <div class="barWrapper">
                    <span class="progressText">Corel Draw</span>
                    <div class="single-progress-txt">
                      <div class="progress ">
                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="10" aria-valuemax="100" style="">
                            
                        </div>
                      </div>
                      <h3>60%</h3>  
                    </div>
                  </div><!-- /.barWrapper -->
                  <div class="barWrapper">
                    <span class="progressText">Adobe Illustrator</span>
                    <div class="single-progress-txt">
                      <div class="progress">
                         <div class="progress-bar" role="progressbar" aria-valuenow="65" aria-valuemin="10" aria-valuemax="100" style="">
                            
                         </div>
                      </div>
                      <h3>65%</h3>  
                    </div>
                  </div><!-- /.barWrapper -->
                  <div class="barWrapper">
                    <span class="progressText">Adobe Photoshop</span>
                    <div class="single-progress-txt">
                      <div class="progress">
                         <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="10" aria-valuemax="100" style="">
                            
                         </div>
                      </div>
                      <h3>50%</h3>  
                    </div>
                  </div><!-- /.barWrapper -->
                  <div class="barWrapper">
                    <span class="progressText">UI/UX DESIGN</span>
                    <div class="single-progress-txt">
                      <div class="progress ">
                         <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="10" aria-valuemax="100" style="">
                           
                         </div>
                      </div>
                      <h3>85%</h3>  
                    </div>
                  </div><!-- /.barWrapper -->
                  <div class="barWrapper">
                    <span class="progressText">JavaScript</span>
                    <div class="single-progress-txt">
                      <div class="progress ">
                         <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="10" aria-valuemax="100" style="">
                            
                         </div>
                      </div>
                      <h3>50%</h3>  
                    </div>
                  </div><!-- /.barWrapper -->
                </div>
              </div>
              <div class="col-md-4">
                <div class="single-skill-content">
                  <div class="barWrapper">
                    <span class="progressText">HTML</span>
                    <div class="single-progress-txt">
                      <div class="progress ">
                        <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="10" aria-valuemax="100" style="">
                          
                        </div>
                      </div>
                      <h3>80%</h3>  
                    </div>
                  </div><!-- /.barWrapper -->
                  <div class="barWrapper">
                    <span class="progressText">CSS</span>
                    <div class="single-progress-txt">
                      <div class="progress ">
                         <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="10" aria-valuemax="100" style="">
                            
                         </div>
                      </div>
                      <h3>70%</h3>  
                    </div>
                  </div><!-- /.barWrapper -->
                  <div class="barWrapper">
                    <span class="progressText">Communication</span>
                    <div class="single-progress-txt">
                      <div class="progress ">
                         <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="10" aria-valuemax="100" style="">
                           
                         </div>
                      </div>
                      <h3>80%</h3>  
                    </div>
                  </div><!-- /.barWrapper -->
                  <div class="barWrapper">
                    <span class="progressText"> Creativity</span>
                    <div class="single-progress-txt">
                      <div class="progress ">
                         <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="10" aria-valuemax="100" style="">
                            
                         </div>
                      </div>
                      <h3>80%</h3>  
                    </div>
                  </div><!-- /.barWrapper -->
                   <div class="barWrapper">
                    <span class="progressText">Android Studio</span>
                    <div class="single-progress-txt">
                      <div class="progress ">
                         <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="10" aria-valuemax="100" style="">
                            
                         </div>
                      </div>
                      <h3>50%</h3>  
                    </div>
                  </div><!-- /.barWrapper -->
                </div>
              </div>
               <div class="col-md-4">
                <div class="single-skill-content">
                  <div class="barWrapper">
                    <span class="progressText">Microsoft Word</span>
                    <div class="single-progress-txt">
                      <div class="progress ">
                        <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="10" aria-valuemax="100" style="">
                          
                        </div>
                      </div>
                      <h3>95%</h3>  
                    </div>
                  </div><!-- /.barWrapper -->
                  <div class="barWrapper">
                    <span class="progressText">Microsoft Excel</span>
                    <div class="single-progress-txt">
                      <div class="progress ">
                         <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="10" aria-valuemax="100" style="">
                            
                         </div>
                      </div>
                      <h3>90%</h3>  
                    </div>
                  </div><!-- /.barWrapper -->
                  <div class="barWrapper">
                    <span class="progressText">PHP</span>
                    <div class="single-progress-txt">
                      <div class="progress ">
                         <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="10" aria-valuemax="100" style="">
                           
                         </div>
                      </div>
                      <h3>60%</h3>  
                    </div>
                  </div><!-- /.barWrapper -->
                  <div class="barWrapper">
                    <span class="progressText"> Basis Data</span>
                    <div class="single-progress-txt">
                      <div class="progress ">
                         <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="10" aria-valuemax="100" style="">
                            
                         </div>
                      </div>
                      <h3>60%</h3>  
                    </div>
                  </div><!-- /.barWrapper -->
                   <div class="barWrapper">
                    <span class="progressText">Microsoft Power Point</span>
                    <div class="single-progress-txt">
                      <div class="progress ">
                         <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="10" aria-valuemax="100" style="">
                            
                         </div>
                      </div>
                      <h3>80%</h3>  
                    </div>
                  </div><!-- /.barWrapper -->
                </div>
              </div>
            </div><!-- /.row -->
          </div>  <!-- /.container -->    
        </div><!-- /.skill-content-->

    </section><!--/.skills-->
    <!--skills end -->

    <!--experience start -->
    <section id="experience" class="experience">
      <div class="section-heading text-center">
        <h2>experience & organization</h2>
      </div>
      <div class="container">
        <div class="experience-content">
            <div class="main-timeline">
              <ul>
                <li>
                  <div class="single-timeline-box fix">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="experience-time text-right">
                          <h2>Juny 2018 - August 2018</h2>
                          <h3>UI/UX and Frontend Dev</h3>
                        </div><!--/.experience-time-->
                      </div><!--/.col-->
                      <div class="col-md-offset-1 col-md-5">
                        <div class="timeline">
                          <div class="timeline-content">
                            <h4 class="title">
                              <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                              Qiscus(Chat SDK FOR Mobile or Website)
                            </h4>
                            <h5>Yogyakarta City, Central Java</h5>
                            <p class="description">
                              As UI/UX Design And Frontend Dev in Qiscus Trello Application
                            </p>
                          </div><!--/.timeline-content-->
                        </div><!--/.timeline-->
                      </div><!--/.col-->
                    </div>
                  </div><!--/.single-timeline-box-->
                </li>

                <li>
                  <div class="single-timeline-box fix">
                    <div class="row">
                      <div class="col-md-offset-1 col-md-5 experience-time-responsive">
                        <div class="experience-time">
                          <h2>
                            <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                            August 2017 - December 2017
                          </h2>
                          <h3>Laboratory Assistant</h3>
                        </div><!--/.experience-time-->
                      </div><!--/.col-->
                      <div class="col-md-5">
                        <div class="timeline">
                          <div class="timeline-content text-right">
                            <h4 class="title">
                              Laboratory Assistant
                            </h4>
                            <h5>Yogyakarta City, Central Java</h5>
                            <p class="description">
                              Responsible for assisting the students in Computer Installation Management at Computer and Information System Laboratory, Vocational College, Universitas Gadjah Mada.
                            </p>
                          </div><!--/.timeline-content-->
                        </div><!--/.timeline-->
                      </div><!--/.col-->
                      <div class="col-md-offset-1 col-md-5 experience-time-main">
                        <div class="experience-time">
                          <h2>
                            <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                             August 2017 - December 2017
                          </h2>
                          <h3>lABORATORY ASSISTANT</h3>
                        </div><!--/.experience-time-->
                      </div><!--/.col-->
                    </div>
                  </div><!--/.single-timeline-box-->
                </li>

                <li>
                  <div class="single-timeline-box fix">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="experience-time text-right">
                          <h2>27-28 September 2018</h2>
                          <h3></h3>
                        </div><!--/.experience-time-->
                      </div><!--/.col-->
                      <div class="col-md-offset-1 col-md-5">
                        <div class="timeline">
                          <div class="timeline-content">
                            <h4 class="title">
                              <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                              INDONESIA ANDROID KEJAR (BEGINNER)
                            </h4>
                            <h5>Yogyakarta City, Central Java</h5>
                            <p class="description">
                             
                            </p>
                          </div><!--/.timeline-content-->
                        </div><!--/.timeline-->
                      </div><!--/.col-->
                    </div>
                  </div><!--/.single-timeline-box-->
                </li>

                <li>
                  <div class="single-timeline-box fix">
                    <div class="row">
                      <div class="col-md-offset-1 col-md-5 experience-time-responsive">
                        <div class="experience-time">
                          <h2>
                            <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                            2010 - 2012
                          </h2>
                          <h3>Student Council of SMPN 2 KOTA CIREBON</h3>
                        </div><!--/.experience-time-->
                      </div><!--/.col-->
                      <div class="col-md-5">
                        <div class="timeline">
                          <div class="timeline-content text-right">
                            <h4 class="title">
                              TREASURER I
                            </h4>
                            <h5>Cirebon City, West Java</h5>
                            <p class="description">
                            </p>
                          </div><!--/.timeline-content-->
                        </div><!--/.timeline-->
                      </div><!--/.col-->
                      <div class="col-md-offset-1 col-md-5 experience-time-main">
                        <div class="experience-time">
                          <h2>
                            <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                            2010-2012
                          </h2>
                          <h3>Student Council of SMPN 2 KOTA CIREBON</h3>
                        </div><!--/.experience-time-->
                      </div><!--/.col-->
                    </div>
                  </div><!--/.single-timeline-box-->
                </li>

                <li>
                  <div class="single-timeline-box fix">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="experience-time text-right">
                          <h2>2016 - Now</h2>
                          <h3>Division of entrepreneurship</h3>
                        </div><!--/.experience-time-->
                      </div><!--/.col-->
                      <div class="col-md-offset-1 col-md-5">
                        <div class="timeline">
                          <div class="timeline-content">
                            <h4 class="title">
                              <span><i class="fa fa-circle" aria-hidden="true"></i></span>
                             HIMAKOMSI(HIMPUNAN MAHASISWA ILMU KOMPUTER DAN SISTEM INFORMASI)
                            </h4>
                            <h5>Computer and Information System , UGM</h5>
                            <p class="description">
                            </p>
                          </div><!--/.timeline-content-->
                        </div><!--/.timeline-->
                      </div><!--/.col-->
                    </div>
                  </div><!--/.single-timeline-box-->
                </li>

              </ul>
            </div><!--.main-timeline-->
          </div><!--.experience-content-->
      </div>

    </section><!--/.experience-->
    <!--experience end -->

    <!--profiles start -->
    <section id="profiles" class="profiles">
      <div class="profiles-details">
        <div class="section-heading text-center">
          <h2>profiles</h2>
        </div>
        <div class="container">
          <div class="profiles-content">
            <div class="row">
              <div class="col-sm-3">
                <div class="single-profile">
                  <div class="profile-txt">
                    <a href="https://www.figma.com/files/recent"><i class="flaticon-themeforest"></i></a>
                    <div class="profile-icon-name">Figma</div>
                  </div>
                  <div class="single-profile-overlay">
                    <div class="profile-txt">
                      <a href="https://www.figma.com/files/recent"><i class="flaticon-themeforest"></i></a>
                      <div class="profile-icon-name">Figma</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="single-profile">
                  <div class="profile-txt">
                    <a href="https://dribbble.com/sitinoraika"><i class="flaticon-dribbble"></i></a>
                    <div class="profile-icon-name">dribbble</div>
                  </div>
                  <div class="single-profile-overlay">
                    <div class="profile-txt">
                      <a href="https://dribbble.com/sitinoraika"><i class="flaticon-dribbble"></i></a>
                      <div class="profile-icon-name">dribbble</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="single-profile">
                  <div class="profile-txt">
                    <a href="https://www.behance.net/sitinoraik0530"><i class="flaticon-behance-logo"></i></a>
                    <div class="profile-icon-name">behance</div>
                  </div>
                  <div class="single-profile-overlay">
                    <div class="profile-txt">
                      <a href="https://www.behance.net/sitinoraik0530"><i class="flaticon-behance-logo"></i></a>
                      <div class="profile-icon-name">behance</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="single-profile profile-no-border">
                  <div class="profile-txt">
                    <a href=""><i class="flaticon-github-logo"></i></a>
                    <div class="profile-icon-name">github</div>
                  </div>
                  <div class="single-profile-overlay">
                    <div class="profile-txt">
                      <a href=""><i class="flaticon-github-logo"></i></a>
                      <div class="profile-icon-name">github</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="profile-border"></div>
           
          </div>
        </div>
      </div>

    </section><!--/.profiles-->
    <!--profiles end -->

    <!--portfolio start -->
    <section id="portfolio" class="portfolio">
      <div class="portfolio-details">
        <div class="section-heading text-center">
          <h2>portofolio</h2>
        </div>
        <div class="container">
          <div class="portfolio-content">
            <div class="isotope">
              <div class="row">

                <div class="col-sm-4">
                  <div class="item">
                    <img src="assets/images/portfolio/Capture1.png" alt=""/>
                    <div class="isotope-overlay">
                      <a href="#">
                        ui/ux design
                      </a>
                    </div><!-- /.isotope-overlay -->
                  </div><!-- /.item -->
                  <div class="item">
                    <img src="assets/images/portfolio/Capture2.png" alt=""/>
                    <div class="isotope-overlay">
                      <a href="#">
                        ui/ux design
                      </a>
                    </div><!-- /.isotope-overlay -->
                  </div><!-- /.item -->
                </div><!-- /.col -->

                <div class="col-sm-4">
                  <div class="item">
                    <img src="assets/images/portfolio/Capture.png" alt=""/>
                    <div class="isotope-overlay">
                      <a href="#">
                        Desain Graphic
                      </a>
                    </div><!-- /.isotope-overlay -->
                  </div><!-- /.item -->
                </div><!-- /.col -->

                <div class="col-sm-4">
                  <div class="item">
                    <img src="assets/images/portfolio/lowpoly.png" alt=""/>
                    <div class="isotope-overlay">
                      <a href="#">
                        LowPoly Art
                      </a>
                    </div><!-- /.isotope-overlay -->
                  </div><!-- /.item -->
                  <div class="item">
                    <img src="assets/images/portfolio/kapsul1.png" alt=""/>
                    <div class="isotope-overlay">
                      <a href="#">
                        Inkspace
                      </a>
                    </div><!-- /.isotope-overlay -->
                  </div><!-- /.item -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!--/.isotope-->
          </div><!--/.gallery-content-->
        </div><!--/.container-->
      </div><!--/.portfolio-details-->

    </section><!--/.portfolio-->
    <!--portfolio end -->

    <!--clients start -->
    <section id="clients" class="clients">
      <div class="section-heading text-center">
        <h2>PROJECT</h2>
      </div>
        <div class="container">
          <div class="profiles-content">
            <div class="row">
              <div class="col-sm-3">
                <div class="single-profile">
                  <div class="profile-txt">
                    <a href="http://egama.herokuapp.com/"></i></a>
                    <div class="profile-icon-name">UI/UX DESIGN AND FRONTEND DEV</div>
                  </div>
                  <div class="single-profile-overlay">
                    <div class="profile-txt">
                      <a href="http://egama.herokuapp.com/"></i></a>
                      <div class="profile-icon-name">egama.herokuapp.com</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="single-profile">
                  <div class="profile-txt">
                    <a href="http://qiscusdev.herokuapp.com/"></i></a>
                    <div class="profile-icon-name">UI/UX DESIGN AND FRONTEND DEV</div>
                  </div>
                  <div class="single-profile-overlay">
                    <div class="profile-txt">
                      <a href="http://qiscusdev.herokuapp.com/"></i></a>
                      <div class="profile-icon-name">qiscusdev.herokuapp.com</div>
                    </div>
                  </div>
                </div>
              </div>
             
              
            </div>
            
            
          </div>
        </div>
    

  

    </section><!--/.clients-->

    </section><!--/.clients-->
    <!--clients end -->

    <!--contact start -->
    <section id="contact" class="contact">
      <div class="section-heading text-center">
        <h2>contact me</h2>
      </div>
      <div class="container">
        <div class="contact-content">
          <div class="row">
            <div class="col-md-offset-1 col-md-5 col-sm-6">
              <div class="single-contact-box">
                <div class="contact-form">
                  <form>
                    <div class="row">
                      <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                          <input type="text" class="form-control" id="name" placeholder="Name*" name="name">
                        </div><!--/.form-group-->
                      </div><!--/.col-->
                      <div class="col-sm-6 col-xs-12">
                        <div class="form-group">
                          <input type="email" class="form-control" id="email" placeholder="Email*" name="email">
                        </div><!--/.form-group-->
                      </div><!--/.col-->
                    </div><!--/.row-->
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <input type="text" class="form-control" id="subject" placeholder="Subject" name="subject">
                        </div><!--/.form-group-->
                      </div><!--/.col-->
                    </div><!--/.row-->
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <textarea class="form-control" rows="8" id="comment" placeholder="Message" ></textarea>
                        </div><!--/.form-group-->
                      </div><!--/.col-->
                    </div><!--/.row-->
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="single-contact-btn">
                          <a class="contact-btn" href="#" role="button">submit</a>
                        </div><!--/.single-single-contact-btn-->
                      </div><!--/.col-->
                    </div><!--/.row-->
                  </form><!--/form-->
                </div><!--/.contact-form-->
              </div><!--/.single-contact-box-->
            </div><!--/.col-->
            <div class="col-md-offset-1 col-md-5 col-sm-6">
              <div class="single-contact-box">
                <div class="contact-adress">
                  <div class="contact-add-head">
                    <h3>Siti Noraika</h3>
                    <p>uI/uX Enthusiast</p>
                  </div>
                  <div class="contact-add-info">
                    <div class="single-contact-add-info">
                      <h3>phone</h3>
                      <p>0895-3209-03454</p>
                    </div>
                    <div class="single-contact-add-info">
                      <h3>email</h3>
                      <p>noraika.siti@gmail.com</p>
                    </div>
                    <div class="single-contact-add-info">
                      <h3>website</h3>
                      <p>sitinoraika.herokuapp.com</p>
                    </div>
                  </div>
                </div><!--/.contact-adress-->
                <div class="hm-foot-icon">
                  <ul>
                    <li><a href="https://www.facebook.com/ika.ikolpartiii"><i class="fa fa-facebook"></i></a></li><!--/li-->
                    <li><a href="https://dribbble.com/sitinoraika"><i class="fa fa-dribbble"></i></a></li><!--/li-->
                  
                    <li><a href="https://www.linkedin.com/in/siti-noraika-aa2200135/"><i class="fa fa-linkedin"></i></a></li><!--/li-->
                    <li><a href="https://www.instagram.com/ikanoraika/?hl=id"><i class="fa fa-instagram"></i></a></li><!--/li-->
                  </ul><!--/ul-->
                </div><!--/.hm-foot-icon-->
              </div><!--/.single-contact-box-->
            </div><!--/.col-->
          </div><!--/.row-->
        </div><!--/.contact-content-->
      </div><!--/.container-->

    </section><!--/.contact-->
    <!--contact end -->

    <!--footer-copyright start-->
    <footer id="footer-copyright" class="footer-copyright">
      <div class="container">
        <div class="hm-footer-copyright text-center">
          <p>
            &copy; copyright 2018 All Right Reserved 
          </p><!--/p-->
        </div><!--/.text-center-->
      </div><!--/.container-->

      <div id="scroll-Top">
        <div class="return-to-top">
          <i class="fa fa-angle-up " id="scroll-top" ></i>
        </div>
        
      </div><!--/.scroll-Top-->
      
        </footer><!--/.footer-copyright-->
    <!--footer-copyright end-->
    
    <!-- Include all js compiled plugins (below), or include individual files as needed -->

    <script src="{{ asset('/assets/js/jquery.js') }}"></script>
        
        <!--modernizr.min.js-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    
    <!--bootstrap.min.js-->
        <script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
    
    <!-- bootsnav js -->
    <script src="{{ asset('/assets/js/bootsnav.js') }}"></script>
    
    <!-- jquery.sticky.js -->
    <script src="{{ asset('/assets/js/jquery.sticky.js') }}"></script>
    
    <!-- for progress bar start-->

    <!-- progressbar js -->
    <script src="{{ asset('/assets/js/progressbar.js') }}"></script>

    <!-- appear js -->
    <script src="{{ asset('/assets/js/jquery.appear.js') }}"></script>

    <!-- for progress bar end -->

    <!--owl.carousel.js-->
        <script src="{{ asset('/assets/js/owl.carousel.min.js') }}"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    
        
        <!--Custom JS-->
        <script src="{{ asset('/assets/js/custom.js') }}"></script>
        
    </body>
  
</html>